from turtle import *

def quadrat(seitenlänge, stiftfarbe, füllfarbe):
    pencolor(stiftfarbe)
    fillcolor(füllfarbe)
    begin_fill()
    forward(seitenlänge)
    left(90)
    forward(seitenlänge)
    left(90)
    forward(seitenlänge)
    left(90)
    forward(seitenlänge)
    left(90)
    end_fill()

def treppe(seitenlänge, ff1, ff2, ff3):
    quadrat(seitenlänge, "red", ff1)
    forward(seitenlänge)
    left(90)
    forward(seitenlänge)
    right(90)
    quadrat(seitenlänge, "red", ff2)  
    forward(seitenlänge)
    left(90)
    forward(seitenlänge)
    right(90)
    quadrat(seitenlänge, "red", ff3)
    
treppe(10, "lime", "yellow", "blue")
treppe(20, "green", "cyan", "orange")
