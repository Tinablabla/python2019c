name=input("Enter Name? ")
#print("Printing Details")
#print ("name")
print (name)

name = name + " "
print (name * 5)

betrag=input("Betrag?")
betrag=float(betrag)

print(betrag*7.7/100, "CHF")

# liefert einen fehler, da man eine kommazahl und einen text
# nicht addieren kann.
betrag_als_text = str(betrag*7.7/100)
print(betrag_als_text + " CHF")


# String slicing
text = "Donaudampfschiffsfahrtgesellschaftskapitän"

# gesuchte ausgabe: schiff
schiff = "Donaudampfschiffsfahrtgesellschaftskapitän" [10:16]
print(dir(schiff))
print()
print(schiff.upper())
print(schiff.capitalize())
print()

