from turtle import *
reset()
pencolor("red")
pensize(5)
fillcolor("cyan")
begin_fill()
left(30)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()
left(180)
forward(100)
fillcolor("yellow")
begin_fill()
left(110)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()
fillcolor("pink")
begin_fill()
left(30)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()
fillcolor("blue")
begin_fill()
left(10)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()
fillcolor("green")
begin_fill()
left(100)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
end_fill()


