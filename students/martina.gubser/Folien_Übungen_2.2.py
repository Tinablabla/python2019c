from turtle import *

dreieckeins =numinput("Blaues Dreieck", 'Bitte geben sie die Länge ein')
dreieckzwei =numinput("Gelbes Dreieck", 'Bitte geben sie die Länge des grossen Dreiecks ein')
dreieckdrei =numinput("Grünes Dreieck", 'Bitte geben sei die Länge des kleinen Dreiecks ein')

reset()

def dreieck(fuellfarbe,seitenlaenge):
    fillcolor(fuellfarbe)
    begin_fill()
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    end_fill()

pensize(5)
pencolor("red")
right(90)

dreieck("cyan", dreieckeins)
dreieck("yellow", dreieckzwei)
dreieck("lime", dreieckdrei)