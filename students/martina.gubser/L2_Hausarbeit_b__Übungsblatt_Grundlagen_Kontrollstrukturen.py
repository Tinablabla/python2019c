
#2.1
def guten_morgen():
    print("Guten Morgen!")
    
guten_morgen()

#2.2
def guten_morgen(name):
    print("Guten Morgen",name+"!")
   
guten_morgen("Ana")

#2.3 a)
def guten_morgen(name):
    return "Guten Morgen "+ name+"!"

gruss = guten_morgen("Ana")
print(gruss)


#2.3 b)
def flaeche_rechteck(breite,laenge):
    flaeche=breite*laenge
    return flaeche

flaeche = flaeche_rechteck(10,20)
print("Die Fläche beträgt", flaeche, "m2.")


    


    


