from turtle import*
name="turtle"

right(30)

begin_fill()
pencolor("red")
fillcolor("cyan")
pensize(5)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
right(90)
end_fill() 

left(300)

begin_fill()
pencolor("red")
pensize(5)
fillcolor("blue")
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
right(90)
end_fill()


left(120)

begin_fill()
pencolor("red")
fillcolor("lightgreen")
pensize(5)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
right(90)
end_fill()

left(50)

begin_fill()
pencolor("red")
fillcolor("yellow")
pensize(5)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
right(90)
end_fill()

left(80)

begin_fill()
pencolor("red")
fillcolor("pink")
pensize(5)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
right(90)
end_fill()







